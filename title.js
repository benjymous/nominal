var Util = require("util");
var Escape = require("escape");
var state = require("AppState");

var exit = false;
var frameNum = 0;

exports.enter = function() {
    Screen.showCursor(false);
    Screen.showMouse(MouseImage.HIDDEN);
    Screen.clearWindow();
    Screen.cls(0, 1, 1, 0, 0, 7);
    Sprite.clear();
    
    IO.loadTilePages("nominal","23 4567 89AB CDEF");
    
    var exit = false;
    
    // Prism splash
    Util.loadScreen("nominal.txt", 32,54, 32, 8, -1, -1, 0);
    
}

exports.onKey = function (key) {
    exit = true;
}

exports.update = function() {
    frameNum++;

    if (frameNum == 5 * 1000 * 60) {
        /// Title screen
        Util.loadScreen("nominal.txt", 14,24, Screen.width, Screen.height, 0, 0, 0);
    }
    
    if (frameNum == 10 * 1000 * 60) {
        /// Title text
        Util.loadScreen("nominal.txt", 21,37, 16,8, 7,13, -5);
        Util.loadScreen("nominal.txt", 48,4, 16,8, 6,12, 0);
    
        Screen.locate(15, 25);
        Screen.print(Escape.paper(0)+Escape.ink(2)+"A GAME ABOUT ORBITS");
    }
    
    if (frameNum == 15 * 1000 *60) {
        /// Exit
        exit = true;
    }

    if (exit) {
        Screen.cls(0, 1, 1, 0, 0, 7);
        state.enter("menu");
    }
}