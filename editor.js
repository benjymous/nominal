var Util = require("util");
var Graviton = require("graviton");

var shipFrames, propData, planetData, planetSchemes;
var frameCount = 0;

exports.enter = function() {
    IO.loadTilePages("nominal","23 4567 89AB CDEF");

    shipFrames = Util.loadJson("shipsprite.json");
    propData = Util.loadJson("props.json");
    planetData = Util.loadJson("planets.json");
    planetSchemes = Util.loadJson("planetSchemes.json");

    frameCount = 0;
    
    initLevel(level);
    showSelection();
}

function toRadians(angle) {
    return angle * (Math.PI / 180);
}

var radians_90deg = toRadians(90);
var radians_180deg = toRadians(180);
var radians_270deg = toRadians(270);
var radians_360deg = toRadians(360);


var SPRINDEX = 500;

var filename = "level.json";

var level = {

    props: [
        {
            name: "ship",
            position: [100, 100],
        },
    ],

    planets: [
        {
            data: 0,
            scheme: 0,
            position: [192, 108]
        }
    ],
}


var selected = undefined;
var Trace = undefined;
var props = [];
var planets = [];

var maxTrace = 300;

var traceSpr = 0;
var resetTrace = true;


function save(filename) {
    level = {

        props: props,

        planets: planets,
    }

    System.debug(level.props.length + " props, " + level.planets.length + " planets saved");

    Util.saveJson(filename, level);
}

function load(filename) {
    level = Util.loadJson(filename);

    System.debug(level.props.length + " props, " + level.planets.length + " planets loaded");

    initLevel(level);
}

function initLevel(level) {

    Sprite.clear();
    Screen.showCursor(false);
    Screen.showMouse(MouseImage.HIDDEN);
    Screen.clearWindow();
    Screen.cls(0, 1, 1, 0, 0, 7);

    props = [];
    planets = [];

    SPRINDEX = 500;

    Graviton.reset();

    level.props.forEach(function (obj) {
        System.debug("Restoring "+obj.name);
        if (obj.velocity != undefined) {
            Graviton.restoreProp(obj, SPRINDEX--);
        }
        else {
            Graviton.createProp(name = obj.name, type = propData[obj.name], position = obj.position, spriteIndex = SPRINDEX--);
        }
    });

    level.planets.forEach(function (obj) {
        var planet = Graviton.createPlanet("Planet", planetData[obj.data], planetSchemes[obj.scheme], obj.position);
        planet.data = obj.data;
        planet.scheme = obj.scheme;
        planets.push(planet);
    });

    if (props.length > 0) {
        selected = props[0];
    }
    resetTrace = true;

    Trace = Graviton.createObject(name = "Trace", position = [192.0, 30.0], radius = 8, mass = 1.0e4, fixed = false, influence = false);
}

function showSelection() {

    //Screen.locate(0,0);
    //Screen.print(selected.name+" "+selected.velocity+" "+selected.motionDirection+"    ");

    if (selected != undefined && frameCount % 30 < 15) {
        Sprite.set(0, 7, 7, 2, 2, 26);
        Sprite.move(0, selected.position[0] - 4, selected.position[1] - 4)

        if (selected.velocity != 0) {
            var x = selected.position[0];
            var y = selected.position[1];

            x += Math.sin(selected.motionDirection) * selected.velocity / 500.0
            y += Math.cos(selected.motionDirection) * selected.velocity / 500.0

            Sprite.set(1, 7, 7, 2, 3, 26);
            Sprite.move(1, x - 4, y - 4);
        }
    }
    else {
        Sprite.clear(0);
        Sprite.clear(1);
    }
}

exports.onKey = function (key) {

    if (key.Str == 's') {
        save(filename);
    }

    if (key.Str == 'l') {
        load(filename);
    }

    if (key.Str == '\t') {
        System.debug("TAB");
        var i = props.indexOf(selected);
        System.debug("Current "+i);
        i = (i + 1) % props.length;
        System.debug("New "+i);
        selected = props[i];
    }

    var num = parseInt(key.Str);
    if (num != NaN) {
        var keys = Object.keys(propData);
        if (num < keys.length) {
            
            var name = keys[num]
            System.debug("Adding " + name);
            var data = propData[name];
            selected = Graviton.createProp(name = name, type = data, position = [100, 100], spriteIndex = SPRINDEX--);
        }
    }

    if (selected == undefined) {
        return;
    }

    if (key.Mod == Keymod.NONE) {
        if (key.Code == Keycode.LEFT) {
            selected.position[0]--;
        }

        if (key.Code == Keycode.RIGHT) {
            selected.position[0]++;
        }

        if (key.Code == Keycode.UP) {
            selected.position[1]++;
        }

        if (key.Code == Keycode.DOWN) {
            selected.position[1]--;
        }
    }
    else {
        if (key.Code == Keycode.LEFT) {
            selected.giveMotion(deltaV = 250.0, motionDirection = selected.motionDirection - radians_90deg);
        }

        if (key.Code == Keycode.RIGHT) {
            selected.giveMotion(deltaV = 250.0, motionDirection = selected.motionDirection + radians_90deg);
        }

        if (key.Code == Keycode.UP) {
            selected.giveMotion(deltaV = 250.0, motionDirection = selected.motionDirection);
        }

        if (key.Code == Keycode.DOWN) {
            selected.giveMotion(deltaV = 250.0, motionDirection = selected.motionDirection + radians_180deg);
        }
    }

    selected.draw();
    showSelection();
    resetTrace = true;



}

exports.update = function () {
    frameCount++;
    showSelection();

    if (traceSpr == maxTrace || resetTrace) {
        Trace.clone(selected);
        traceSpr = 0;
        resetTrace = false;
    }

    Trace.vectorUpdate(60);
    Sprite.set(traceSpr, 7, 7, 1, 0, 24, 8);
    Sprite.move(traceSpr++, Trace.position[0], Trace.position[1]);

    var d = Graviton.dist(Trace.position, selected.position)
    if (traceSpr > 10 && d < 8) {
        resetTrace = true;
        for (var i = traceSpr; i < maxTrace; ++i) {
            Sprite.clear(i);
        }
    }
};

