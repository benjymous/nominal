exports.G = 6.673e-11  // gravity constant
exports.gridScale = 50000; // 1 unit of grid equals 1000000m or 1000km

exports.instances = [];

exports.reset = function () {
    module.exports.instances = [];
}

exports.createObject = function (name, position, radius, mass, fixed, influence) {
    var obj = {
        name: name,
        position: position,
        radius: radius,
        mass: mass,
        velocity: 0,
        time: 0,
        motionDirection: 0,
        fixed: fixed,
        influence: influence,
        crashed: false
    };
    return module.exports.restoreObject(obj);
}

exports.restoreObject = function(obj) {

    System.debug("Added " + obj.name);
    module.exports.instances.push(obj);

    obj.clone = function (other) {
        self = this;
        
        self.radius = other.radius;
        self.position = other.position;
        self.velocity = other.velocity;
        self.motionDirection = other.motionDirection;
        self.crashed = other.crashed;
    }

    obj.giveMotion = function (deltaV, motionDirection, time) {
        var self = this;
        if (self.fixed) { return; }
        var x_comp, y_comp;
        if (self.velocity != 0) {
            x_comp = Math.sin(self.motionDirection) * self.velocity;
            y_comp = Math.cos(self.motionDirection) * self.velocity;
            x_comp += Math.sin(motionDirection) * deltaV;
            y_comp += Math.cos(motionDirection) * deltaV;
            self.velocity = Math.sqrt((x_comp ** 2) + (y_comp ** 2));

            if (x_comp > 0 && y_comp > 0) { // calculate degrees depending on the coordinate quadrant
                self.motionDirection = (Math.asin(Math.abs(x_comp) / self.velocity));  // update motion direction
            }
            else if (x_comp > 0 && y_comp < 0) {
                self.motionDirection = (Math.asin(Math.abs(y_comp) / self.velocity)) + radians_90deg;
            }
            else if (x_comp < 0 && y_comp < 0) {
                self.motionDirection = (Math.asin(Math.abs(x_comp) / self.velocity)) + radians_180deg;
            }
            else {
                self.motionDirection = (Math.asin(Math.abs(y_comp) / self.velocity)) + radians_270deg;
            }
        }

        else {
            self.velocity = self.velocity + deltaV;  // in m/s
            self.motionDirection = motionDirection;  // degrees
        }

        return obj;
    }

    obj.vectorUpdate = function (loops) {

        var self = this;

        if (self.fixed) { return; }

        if (loops == undefined) { loops = 1; }

        for (var i = 0; i < loops; ++i) {

            motionForce = self.mass * self.velocity;  // F = m * v

            x_net = 0;
            y_net = 0;

            module.exports.instances.forEach(function (x) {
                if (x == self) { return; }
                if (x.influence == false) { return; }

                //distance = Math.sqrt(((self.position[0]-x.position[0])**2) + (self.position[1]-x.position[1])**2);
                distance = module.exports.dist(self.position, x.position);

                gravityForce = module.exports.G * (self.mass * x.mass) / ((distance * module.exports.gridScale) ** 2);

                if (distance < self.radius + x.radius) {
                    self.crashed = true;
                }

                x_pos = self.position[0] - x.position[0];
                y_pos = self.position[1] - x.position[1];

                if (x_pos <= 0 && y_pos > 0) {  // calculate degrees depending on the coordinate quadrant
                    gravityDirection = (Math.asin(Math.abs(y_pos) / distance)) + radians_90deg;
                }
                else if (x_pos > 0 && y_pos >= 0) {
                    gravityDirection = (Math.asin(Math.abs(x_pos) / distance)) + radians_180deg;
                }
                else if (x_pos >= 0 && y_pos < 0) {
                    gravityDirection = (Math.asin(Math.abs(y_pos) / distance)) + radians_270deg;
                }
                else {
                    gravityDirection = (Math.asin(Math.abs(x_pos) / distance));
                }

                x_gF = gravityForce * Math.sin(gravityDirection);  // x component of vector
                y_gF = gravityForce * Math.cos(gravityDirection);  // y component of vector

                x_net += x_gF;
                y_net += y_gF;
            });

            x_mF = motionForce * Math.sin(self.motionDirection);
            y_mF = motionForce * Math.cos(self.motionDirection);

            x_net += x_mF;
            y_net += y_mF;
            netForce = Math.sqrt((x_net ** 2) + (y_net ** 2));

            if (x_net > 0 && y_net > 0) { // calculate degrees depending on the coordinate quadrant
                self.motionDirection = (Math.asin(Math.abs(x_net) / netForce));  // update motion direction
            }
            else if (x_net > 0 && y_net < 0) {
                self.motionDirection = (Math.asin(Math.abs(y_net) / netForce)) + radians_90deg;
            }
            else if (x_net < 0 && y_net < 0) {
                self.motionDirection = (Math.asin(Math.abs(x_net) / netForce)) + radians_180deg;
            }
            else {
                self.motionDirection = (Math.asin(Math.abs(y_net) / netForce)) + radians_270deg;
            }

            self.velocity = netForce / self.mass;  // update velocity
            traveled = (self.velocity / module.exports.gridScale);  // grid distance traveled per 1 sec

            self.position = [self.position[0] + Math.sin(self.motionDirection) * traveled,
            self.position[1] + Math.cos(self.motionDirection) * traveled];  // update pos

        }

    }

    return obj;
}

exports.addPropData = function(obj, spriteIndex) {
    System.debug("addPropData "+JSON.stringify(obj));
    obj.spriteIndex = spriteIndex;
    props.push(obj);

    obj.draw = function () {
        Sprite.move(this.spriteIndex, this.position[0] - 4, this.position[1] - 4)

        if (this.name == "ship") {
            System.debug("Drawing "+this.name+" at sprIdx "+this.spriteIndex);
            shipFrame = Math.floor((this.motionDirection / radians_360deg) * 64) % 64;

            var frameData = shipFrames[shipFrame];
            Sprite.set(this.spriteIndex, 7, 7, 0, frameData.tx, frameData.ty, 9);
        }

    }

    Sprite.set(spriteIndex, obj.spriteData.c[0], obj.spriteData.c[1], obj.spriteData.c[2], obj.spriteData.t[0], obj.spriteData.t[1]);
    obj.draw();

    return obj;
}

exports.createProp = function (name, type, position, spriteIndex) {
    var obj = module.exports.createObject(name, position, 4, type.mass, false, false);
    obj.spriteData = type;
    
    return module.exports.addPropData(obj, spriteIndex);
}

exports.restoreProp = function(obj, spriteIndex) {
    module.exports.restoreObject(obj);
    return module.exports.addPropData(obj, spriteIndex);
}

exports.createPlanet = function (name, type, colour, position) {
    var obj = module.exports.createObject(name, position, type.radius, type.mass, fixed = true, influence = true);


    var tx = ((position[0] / 8) - (type.size[0] / 2));
    var ty = ((position[1] / 8) - (type.size[1] / 2));

    obj.data = type.index;
    //obj.scheme = colour.index;


    // Draw planet
    //var tx = (Screen.width/2)-4;
    //var ty = (Screen.height/2)-4;

    for (var y = 0; y < type.size[1]; ++y) {
        for (var x = 0; x < type.size[0]; ++x) {
            Tile.set(x + tx, y + ty, colour[0], colour[1], colour[2], x + type.pos[0], y + type.pos[1], colour[3]);
        }
    }

    return obj;
}


exports.dist = function(p1, p2) {
    return Math.sqrt(
        ((p1[0] - p2[0]) ** 2) +
        ((p1[1] - p2[1]) ** 2)
    );
}

