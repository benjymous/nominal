// NOMINAL
// A GAME ABOUT ORBITS
// FOR A GAME BY ITS COVER JAM 2018
//

var Util = require("util");
var Graviton = require("graviton");

var shipFrames, propData, planetData, planetSchemes;

var shipFrame
var ticks = 0;

var SHIPSPRITE = 1;

var spin = 0;

var orbitFrame = 0;
var orbitMax = 500;


var props = [];

var firstTrace = 100;
var maxTrace = 300;

var frameCount = 0;
var traceSpr = 0;
var resetTrace = true;

var firstParticle = firstTrace + maxTrace + 1;
var maxParticles = 30;
var maxParticleFrame = 6;
var particles = [];
var posHist = [];
var posHistLen = 25;

var freeParticles = [];

var burn = 0;

var Ship;


exports.enter = function() {

    IO.loadTilePages("nominal","23 4567 89AB CDEF");

    shipFrames = Util.loadJson("shipsprite.json");
    propData = Util.loadJson("props.json");
    planetData = Util.loadJson("planets.json");
    planetSchemes = Util.loadJson("planetSchemes.json");

    Screen.showCursor(false);
    Screen.showMouse(MouseImage.HIDDEN);
    Screen.clearWindow();
    Screen.cls(0, 1, 1, 0, 0, 7);
    Sprite.clear();

    shipFrame = 0;
    ticks = 0;

    SHIPSPRITE = 1;

    spin = 0;

    orbitFrame = 0;
    orbitMax = 500;

    props = [];

    firstTrace = 100;
    maxTrace = 300;

    frameCount = 0;
    traceSpr = 0;
    resetTrace = true;

    firstParticle = firstTrace + maxTrace + 1;
    maxParticles = 30;
    maxParticleFrame = 6;
    particles = [];
    posHist = [];
    posHistLen = 25;

    freeParticles = [];
    for (var i = firstParticle; i < firstParticle + maxParticles; ++i) {
        freeParticles.push(i);
    }

    burn = 0;


    //Earth = Graviton.createPlanet("Earth", planetData[Util.rndInt(2)], planetSchemes[Util.rndInt(planetSchemes.length)], [192, 108]);
    Earth = Graviton.createPlanet("Earth", planetData[Util.rndInt(2)], planetSchemes[Util.rndInt(planetSchemes.length)], [128, 108]);

    Moon = Graviton.createPlanet("Moon", planetData[4], planetSchemes[Util.rndInt(planetSchemes.length)], [264, 172]);


    //Earth = createPlanet("Earth", planetData[Util.rndInt(planetData.length)], planetSchemes[Util.rndInt(planetSchemes.length)], [192, 108]);

    //Earth = createObject(name="Earth", position=[192.0, 108.0], radius=32, mass=6e24, fixed=true, influence=true);

    Ship = Graviton.createProp(name = "ship", type=propData.ship, position=[152.0, 50.0], spriteIndex = SHIPSPRITE );

    //Ship = Graviton.createObject(name = "Ship", position = [152.0, 50.0], radius = 8, mass = 1.0e4, fixed = false, influence = false);
    Ship.giveMotion(deltaV = 11000.0, motionDirection = toRadians(90));
    Ship.spriteIndex = SHIPSPRITE;

    Graviton.createProp(name = "Orbiter", type = propData.station, position = [60.0, 108.0], spriteIndex = SHIPSPRITE - 1)
        .giveMotion(deltaV = 10000, motionDirection = toRadians(180));

    //createProp(name="Token", type=propData.token, position=[260.0, 120.0], spriteIndex = SHIPSPRITE-2 )
    //        .giveMotion(deltaV = 12000, motionDirection = toRadians(170));

    //SpaceMan = createProp(name="SpaceMan", type=propData.spaceman, position=[320.0, 40.0], spriteIndex = SHIPSPRITE-3 );


    Trace = Graviton.createObject(name = "Trace", position = [192.0, 30.0], radius = 8, mass = 1.0e4, fixed = false, influence = false);

    // Random starfield
    for (var y = 0; y < Screen.height; ++y) {
        for (var x = 0; x < Screen.width; ++x) {
            if (Util.rndInt(10) == 0) {
                Tile.set(x, y, 0, 1, 7, Util.rndInt(64), 22, Util.rndInt(8));
            }
        }
    }


}

function toRadians(angle) {
    return angle * (Math.PI / 180);
}

var radians_90deg = toRadians(90);
var radians_180deg = toRadians(180);
var radians_270deg = toRadians(270);
var radians_360deg = toRadians(360);



exports.onKey = function (key) {
    if (key.Code == Keycode.LEFT) {
        Ship.giveMotion(deltaV = 500.0, motionDirection = Ship.motionDirection - radians_90deg);
        burn = 1;
        resetTrace = true;
    }

    if (key.Code == Keycode.RIGHT) {
        Ship.giveMotion(deltaV = 500.0, motionDirection = Ship.motionDirection + radians_90deg);
        burn = 1;
        resetTrace = true;
    }

    if (key.Code == Keycode.UP) {
        Ship.giveMotion(deltaV = 500.0, motionDirection = Ship.motionDirection);
        burn = 1;
        resetTrace = true;
    }

    if (key.Code == Keycode.DOWN) {
        Ship.giveMotion(deltaV = 500.0, motionDirection = Ship.motionDirection + radians_180deg);
        burn = 1;
        resetTrace = true;
    }
};


function addParticle(position, frame) {

    if (freeParticles.length > 0) {

        var obj = {
            position: [position[0], position[1]],
            frame: frame,
            live: true,
            sprIndex: freeParticles.pop()
        }

        particles.push(obj);
    }
}


function updateParticles() {
    var i = firstParticle;
    var newParticles = [];
    particles.forEach(function (obj) {
        if (obj.live) {
            var f = Math.floor(obj.frame);

            if (f <= maxParticleFrame) {
                Sprite.set(obj.sprIndex, 2, 2, 0, f, 24);
                Sprite.move(obj.sprIndex, obj.position[0] - 3, obj.position[1] - 6);

                obj.frame += 0.05;

                i++;


            }
            else {
                obj.live = false;
            }
            newParticles.push(obj);
        }
        else {
            if (obj.sprIndex != -1) {
                Sprite.clear(obj.sprIndex);
                freeParticles.push(obj.sprIndex);
            }
        }

    });

    particles = newParticles;
}

exports.update = function () {

    frameCount++;

    var start = performance.now();

    Graviton.instances.forEach(function (obj) {
        if (!obj.crashed) {
            obj.vectorUpdate();
        }
    });

    if (burn > 0) {
        if (frameCount % 5 == 0) {
            addParticle(posHist[0], (1 / burn * 2));
            burn -= 0.1;
        }
    }

    //SpaceMan.giveMotion(deltaV = 250, motionDirection = toRadians(Util.rndInt(360)));

    updateParticles();

    if (traceSpr == maxTrace || resetTrace) {
        Trace.clone(Ship);
        traceSpr = firstTrace;
        resetTrace = false;
    }

    shipFrame = Math.floor((Ship.motionDirection / radians_360deg) * 64);

    var frameData = shipFrames[shipFrame];
    
    //Sprite.set(Ship.spriteIndex, 7, 7, 0, frameData.tx, frameData.ty);
    //Sprite.move(Ship.spriteIndex, Ship.position[0] - frameData.xoff, Ship.position[1] - frameData.yoff);

    props.forEach(function (obj) {
        if (!obj.crashed) {
            obj.draw();
        }
        else {
            Sprite.clear(obj.spriteIndex);
        }
    });

    if (!Trace.crashed) {
        Trace.vectorUpdate(60);
        Sprite.set(traceSpr, 7, 7, 1, 0, 24, 8);
        Sprite.move(traceSpr++, Trace.position[0], Trace.position[1]);
    }

    var d = Graviton.dist(Trace.position, Ship.position);
    if (Trace.crashed || (traceSpr > firstTrace+10 && d < 8) ) {
        resetTrace = true;
        for (var i = traceSpr; i < maxTrace; ++i) {
            Sprite.clear(i);
        }
    }
   

    posHistLen = Graviton.gridScale / Ship.velocity * 4;

    posHist.push([Ship.position[0], Ship.position[1]]);
    while (posHist.length > posHistLen) {
        posHist.shift();
    }

    var end = performance.now();

    //System.debug("Frame Time " + Math.round( (end-start) * 100)/100);

};
