
IO.loadTilePages("nominal","23 4567 89AB CDEF");
var strDat = IO.loadtext("shipsprite.json");
var shipFrames = [];
if (strDat != "") {
  shipFrames = JSON.parse(IO.loadtext("shipsprite.json"));
}
else {
  for(i=0; i<64; ++i) {
    shipFrames[i] = {tx: i, ty: 23, xoff:0, yoff:0};
  }
}


Screen.showCursor(false);
Screen.showMouse(MouseImage.CROSSHAIR);
Screen.clearWindow();
Screen.cls(1, 0, 1, 0, 0, 8);

var shipFrame = 0;
var ticks = 0;

var SHIPSPRITE = 510;

shipX = 100;
shipY = 100;

var spin = 0;

App.addEventListener("onMouse", function (button, down, x, y) {
    if (down) {
      shipX = x;
     shipY = y;
    }
});

App.addEventListener("onKey", function (key) {

    var frameData = shipFrames[shipFrame];

  if (key.Str == ',') {
      spin = +63;
  }

  if (key.Str == '.') {
      spin = +1;
  }

  if (key.Code == Keycode.LEFT) {
    frameData.xoff++;
  }

  if (key.Code == Keycode.RIGHT) {
    frameData.xoff--;
  }

  if (key.Code == Keycode.UP) {
    frameData.yoff--;
  }

  if (key.Code == Keycode.DOWN) {
    frameData.yoff++;
  }

  if (key.Str == 'l') {
    shipFrames = JSON.parse(IO.loadtext("shipsprite.json"));
  }

  if (key.Str == 's') {
      IO.savetext("shipsprite.json", JSON.stringify(shipFrames));
  }
});

App.addEventListener("update", function() {

    ticks++;

    if (ticks % 10 == 0) {
        shipFrame = (shipFrame+spin) % 64;
        spin=0;
    }

    var frameData = shipFrames[shipFrame];

    Screen.locate(0,0);
    Screen.print(spin);
    Screen.print(shipFrame);
    Screen.print(JSON.stringify(frameData));

    Sprite.set(SHIPSPRITE, 7, 7, 0, frameData.tx, frameData.ty);
    Sprite.move(SHIPSPRITE, shipX-frameData.xoff, shipY-frameData.yoff );

});