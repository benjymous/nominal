var Util = require("util");
var Escape = require("escape");
var state = require("AppState");

var selection = 0;
var reInit = false;

exports.enter = function() {
    Screen.showCursor(false);
    Screen.showMouse(MouseImage.HIDDEN);
    Screen.clearWindow();
    Screen.cls(0, 1, 1, 0, 0, 7);
    Sprite.clear();

    IO.loadTilePages("nominal","23 4567 89AB CDEF");

    Util.loadScreen("nominal.txt", 14, 24, Screen.width, Screen.height, 0, 0, 0);
    Util.loadScreen("nominal.txt", 21, 37, 16, 8, 7, 13, -5);
    Util.loadScreen("nominal.txt", 48, 4, 16, 8, 6, 12, 0);

    selection = 0;
    reInit = false;

    drawMenu();
}

function drawMenu() {
    Screen.locate(33, 12); Screen.print(Escape.paper(0) + Escape.ink(selection == 0 ? 6 : 2) + "NEW GAME");
    Screen.locate(33, 14); Screen.print(Escape.paper(0) + Escape.ink(selection == 1 ? 6 : 2) + "PICK LEVEL");
    Screen.locate(33, 16); Screen.print(Escape.paper(0) + Escape.ink(selection == 2 ? 6 : 2) + "LEVEL EDITOR");
    Screen.locate(33, 18); Screen.print(Escape.paper(0) + Escape.ink(selection == 3 ? 6 : 2) + "CREDITS");
}

exports.onKey = function (key) {

    if (key.Code == Keycode.UP) {
        selection--;
    }

    if (key.Code == Keycode.DOWN) {
        selection++;
    }

    if (key.Str == " " || key.Code == 13) {
        switch (selection) {
            case 0: 
                state.enter("nominal");
                break;
            case 2: 
                state.enter("editor");

                break;
        }
    }

    if (selection == 4) { selection = 0; }
    if (selection == -1) { selection = 3; }

    drawMenu();
};
